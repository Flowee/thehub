/*
 * This file is part of the Flowee project
 * Copyright (C) 2018-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AbstractCommand.h"
#include "CheckCommand.h"
#include "ExportCommand.h"
#include "InfoCommand.h"
#include "LookupCommand.h"
#include "PruneCommand.h"
#include "DuplicateCommand.h"

#include <QCoreApplication>
#include <QTextStream>
#include <QStandardPaths>

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);
    app.setOrganizationName("flowee");
    app.setOrganizationDomain("flowee.org");
    app.setApplicationName("unspentdb");

    QStringList args = app.arguments();
    if (argc > 2 && args.at(1) == QLatin1String("help")) { // 'help foo' -> 'foo -h'
        args[1] = args[2];
        args[2] = QLatin1String("--help");
    }

    AbstractCommand *run = nullptr;
    for (int i = 1; run == nullptr && i < args.size(); ++i) {
        const QString command = args.at(i);
        if (command == "info")
            run = new InfoCommand();
        else if (command == "prune")
            run = new PruneCommand();
        else if (command == "check")
            run = new CheckCommand();
        else if (command == "export")
            run = new ExportCommand();
        else if (command == "lookup")
            run = new LookupCommand();
        else if (command == "duplicate")
            run = new DuplicateCommand();

        if (run)
            args.takeAt(i); // remove the command
    }

    if (run == nullptr) {
        QTextStream out(stdout);
        out << "Usage unspentdb COMMAND [OPTIONS] ...\n" << Qt::endl;
        out << "Commands:" << Qt::endl;
        out << "  help       Display help for unspentdb or single commands." << Qt::endl;
        out << "Querying the database:" << Qt::endl;
        out << "  lookup     Finds utxo entries." << Qt::endl;
        out << "  info       Prints generic info about a database or part of it." << Qt::endl;
        out << "  export     Exports the entire set of a database." << Qt::endl;
        out << "Database maintainance:" << Qt::endl;
        out << "  check      Checks the internal structures of the database." << Qt::endl;
        out << "  prune      Prunes spent outputs to speed up database usage." << Qt::endl;
        out << "Other:" << Qt::endl;
        out << "  duplicate  Duplicates a file or a directory of the database." << Qt::endl;
        out << Qt::endl;
        return Flowee::InvalidOptions;
    }
    return run->start(args);
}
