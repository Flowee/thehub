# - Try to find Flowee libraries
# Once done this will define
#
#  FLOWEE_FOUND - system has Flowee
#  FLOWEE_INCLUDE_DIR - path to flowee includes
#
# Additionally, the following libraries will be made available;
#  flowee_interfaces
#  flowee_utils
#  flowee_crypto
#  flowee_utils
#  flowee_apputils
#  flowee_networkmanager
#  flowee_p2p
#  flowee_httpengine

# Copyright (c) 2019-2024 Tom Zander <tom@flowee.org>

if (NOT FLOWEE_INCLUDE_DIR)
    find_path(FLOWEE_INCLUDE_DIR flowee/utils/WorkerThreads.h)
    if (NOT FLOWEE_INCLUDE_DIR)
        if (EXISTS ${CMAKE_CURRENT_LIST_DIR}/../include/flowee/utils/WorkerThreads.h)
            set (flowee_ROOT ${CMAKE_CURRENT_LIST_DIR}/..)
            set (FLOWEE_INCLUDE_DIR ${flowee_ROOT}/include)
        endif()
    endif()
endif()

if (NOT flowee_ROOT)
    set (flowee_ROOT ${FLOWEE_INCLUDE_DIR}/..)
endif()


# As one library depends on it below, in case the using-project didn't 'find_package'
# openssl before this is called, we make sure the below still works.
if (NOT "${OPENSSL_FOUND}")
    # If we ever update this file to find only requested flowee libs, this find is
    # only needed if the lib networkmanager is requested.
    find_package(OpenSSL REQUIRED)
endif()


set (__libsFound "NOTFOUND")

set (_floweeSecp256k1 ${flowee_ROOT}/lib/libsecp256k1.a)
if (EXISTS ${_floweeSecp256k1})
    include (${CMAKE_CURRENT_LIST_DIR}/secp256k1.cmake)
    add_library(secp256k1 STATIC IMPORTED)
    set_property(TARGET secp256k1 PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee)
    set_property(TARGET secp256k1 PROPERTY IMPORTED_LINK_INTERFACE_LIBRARIES
        flowee_crypto
        flowee_interfaces
        ${_flowee_secp256k1_GMP_PATH}
    )
    set_target_properties(secp256k1 PROPERTIES IMPORTED_LOCATION ${_floweeSecp256k1})
    set (__libsFound "secp256k1")
endif()

set (_floweeInterfaces ${flowee_ROOT}/lib/libflowee_interfaces.a)
if (EXISTS ${_floweeInterfaces})
    add_library(flowee_interfaces STATIC IMPORTED)
    set_property(TARGET flowee_interfaces PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee/interfaces)
    set_property(TARGET flowee_interfaces APPEND PROPERTY INTERFACE_COMPILE_OPTIONS -fPIC)
    set_target_properties(flowee_interfaces PROPERTIES IMPORTED_LOCATION ${_floweeInterfaces})
    set (__libsFound "${__libsFound} flowee_interfaces")
endif()

set(_floweeCrypto ${flowee_ROOT}/lib/libflowee_crypto.a)
if (EXISTS ${_floweeCrypto})
    add_library(flowee_crypto STATIC IMPORTED)
    set_property(TARGET flowee_crypto PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee/crypto)
    set_property(TARGET flowee_crypto PROPERTY IMPORTED_LINK_INTERFACE_LIBRARIES secp256k1)
    set_target_properties(flowee_crypto PROPERTIES IMPORTED_LOCATION "${_floweeCrypto}")
    set (__libsFound "${__libsFound} flowee_crypto")
endif()

set(_floweeUtils ${flowee_ROOT}/lib/libflowee_utils.a)
if (EXISTS ${_floweeUtils})
    add_library(flowee_utils STATIC IMPORTED)
    set_property(TARGET flowee_utils PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee/utils)
    set_property(TARGET flowee_utils PROPERTY IMPORTED_LINK_INTERFACE_LIBRARIES flowee_crypto flowee_interfaces)
    set_target_properties(flowee_utils PROPERTIES IMPORTED_LOCATION "${_floweeUtils}")
    set (__libsFound "${__libsFound} flowee_utils")
    # openssl is ONLY needed if one uses random.h, so not forcing a linking here.
endif()

set(_floweeAppUtils ${flowee_ROOT}/lib/libflowee_apputils.a)
if (EXISTS ${_floweeAppUtils})
    add_library(flowee_apputils STATIC IMPORTED)
    set_property(TARGET flowee_apputils PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee/apputils)
    set_property(TARGET flowee_apputils PROPERTY IMPORTED_LINK_INTERFACE_LIBRARIES flowee_utils flowee_networkmanager)
    set_target_properties(flowee_apputils PROPERTIES IMPORTED_LOCATION "${_floweeAppUtils}")
    set (__libsFound "${__libsFound} flowee_apputils")
endif()

set(_floweeNetworkManager ${flowee_ROOT}/lib/libflowee_networkmanager.a)
if (EXISTS ${_floweeNetworkManager})
    add_library(flowee_networkmanager STATIC IMPORTED)
    set_property(TARGET flowee_networkmanager PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee/networkmanager)
    set_property(TARGET flowee_networkmanager PROPERTY IMPORTED_LINK_INTERFACE_LIBRARIES ${OPENSSL_LIBRARIES} flowee_utils)
    set_target_properties(flowee_networkmanager PROPERTIES IMPORTED_LOCATION "${_floweeNetworkManager}")
    set (__libsFound "${__libsFound} flowee_networkmanager")
endif()

set(_floweeP2PNet ${flowee_ROOT}/lib/libflowee_p2p.a)
if (EXISTS ${_floweeP2PNet})
    add_library(flowee_p2p STATIC IMPORTED)
    set_property(TARGET flowee_p2p PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/flowee/p2p)
    set_property(TARGET flowee_p2p PROPERTY IMPORTED_LINK_INTERFACE_LIBRARIES flowee_utils flowee_networkmanager)
    set_target_properties(flowee_p2p PROPERTIES IMPORTED_LOCATION "${_floweeP2PNet}")
    set (__libsFound "${__libsFound} flowee_p2p")
endif()

set(_floweeHttpEngine ${flowee_ROOT}/lib/libflowee_httpengine.a)
if (EXISTS ${_floweeHttpEngine})
    add_library(flowee_httpengine STATIC IMPORTED)
    set_property(TARGET flowee_httpengine PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${FLOWEE_INCLUDE_DIR}/httpengine)
    set_target_properties(flowee_httpengine PROPERTIES IMPORTED_LOCATION "${_floweeHttpEngine}")
    set (__libsFound "${__libsFound} flowee_httpengine")
endif()

if (__libsFound)
    add_definitions(-DHAVE_CONFIG_H)
    if ("${CMAKE_BUILD_TYPE}" STREQUAL "Release")
        add_definitions(-DBCH_NO_DEBUG_OUTPUT)
    else ()
        add_definitions(-DBCH_LOGCONTEXT)
    endif ()
    message ("Found Flowee ${CMAKE_CURRENT_LIST_FILE}. Found components: ${__libsFound}")
    set (FLOWEE_FOUND TRUE)
else ()
    if (Flowee_FIND_REQUIRED)
        message(FATAL_ERROR "-- Required component Flowee not found")
    else ()
        message ("No Flowee libs found on system")
    endif ()
endif()
unset(__libsFound)

