/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_SYNCSPVACTION_H
#define FLOWEE_SYNCSPVACTION_H

#include "Action.h"

#include <bloom.h>
#include <map>
#include <random>
#include <boost/date_time/posix_time/posix_time.hpp>

class PrivacySegment;
class Peer;

/**
 * The goal of this action is to make sure all SPV wallets are synched to the chain-tip.
 *
 * This action is started every time when the header is moved. We run every couple of seconds
 * and have the responsibility to start connections to peers and make sure the SPV wallets have
 * good enough data to ensure they have all the information they need using merkleblock downloads.
 */
class SyncSPVAction : public Action
{
public:
    SyncSPVAction(DownloadManager *parent);

protected:
    void execute(const boost::system::error_code &error) override;

private:
    std::random_device m_randomDevice;

    struct PeerDownloadInfo {
        int fromBlock;
        int toBlock; // to-and-including
    };

    struct PeerInfo {
        boost::posix_time::ptime lastCheckedTime;
        int lastHeight = 0; // start height at check time.
        int slowPunishment = 0; // punishment score for being slow

        /**
         * Remember which blocks were downloaded by which peer in order to actually
         * get our security that when we ask multiple peers for the same blocks, we
         * don't mess up and ask the same one twice.
         */
        std::vector<PeerDownloadInfo> previousDownloads;
    };

    std::map<std::shared_ptr<Peer>, PeerInfo> m_peerInfos;
    struct WalletDownloadInfo {
        int connectionsTriedForWallet = 1;
        // The block we're asking the peers to stop downloading at.
        int boundaryHeight = 0;
    };

    std::map<std::shared_ptr<PrivacySegment>, WalletDownloadInfo> m_walletInfos;

    // if nothing happened several calls, we consider us finished
    int m_quietCount = 0;

    const int MinPeersPerWallet;
};

#endif
