/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2024 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_DATALISTENERINTERFACE_H
#define FLOWEE_DATALISTENERINTERFACE_H

#include <deque>
#include <uint256.h>

class Tx;
class BlockHeader;

/**
 * A listener on actions for a specific p2pnet segment.
 *
 * In p2pNet you will have more than one peer for a privacy-segment
 * and you can have any number of privacy segments.
 *
 * A datalistener implements this interface to get notified about actions happening
 * in its context. Most of these methods will be called from the Peer class,
 * one assigned to this context by the DownloadManager.
 *
 * \see DownloadManager::addDataListener(DataListenerInterface *listener)
 * \see PrivacySegment::PrivacySegment()
 */
class DataListenerInterface
{
public:
    DataListenerInterface();
    virtual ~DataListenerInterface();

    /**
     * @brief newTransactions announces a list of transactions pushed to us from a peer.
     * @param blockId the block hash these transactions appeared in.
     * @param blockHeight the blockheight we know the block under.
     * @param blockTransactions The actual transactions.
     */
    virtual void newTransactions(const uint256 &blockId, int blockHeight, const std::deque<Tx> &blockTransactions) = 0;
    /// A single transaction that matches our filters, forwarded to us as it hits a mempool.
    virtual void newTransaction(const Tx &tx) = 0;
    /// notify when we get a newer (higher) blockheight
    virtual void setLastSynchedBlockHeight(int height);
    /// notify when a block has been found (again) from a different peer.
    virtual void updateBackupBlockHeight();

    /**
     * A callback from the p2p lib to the wallet to update and re-create
     * the bloom filter on the PrivacySegment.
     */
    virtual void rebuildFilter();
};

#endif
