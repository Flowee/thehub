/*
 * This file is part of the Flowee project
 * Copyright (C) 2021-2025 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_NOTIFICATIONLISTENER_H
#define FLOWEE_NOTIFICATIONLISTENER_H

#include <cstdint>
#include <vector>
#include <mutex>

namespace P2PNet
{
struct Notification
{
    int blockHeight = -1;
    int txCount = 0;
    int privacySegment = -1;
    int64_t deposited = 0;
    int64_t spent = 0;
};
}

class NotificationListener
{
public:
    NotificationListener();
    virtual ~NotificationListener();

    /**
     * Called by the p2p lib to notify we received a new block moving the 'tip'.
     */
    virtual void notifyNewBlock(const P2PNet::Notification &notification);
    /**
     * A new transaction has been seen on a certain privacy-segment.
     * The parameter notification specifies which segment was updated.
     * All transactions notified to the NotificationCenter are forwarded here.
     */
    virtual void notifyNewTransaction(const P2PNet::Notification &notification);
    /**
     * Notify a communitative notification of all the transactions belonging to
     * a certain privacy segment.
     *
     * When collation is enabled for this listener we combine multiple transactions
     * that all belong to the same 'group' (segmentid) into a single notification.
     * Adding the amount of money added/removed and setting the proper txCount.
     *
     * \see collatedData(), flushCollate();
     */
    virtual void segmentUpdated(const P2PNet::Notification &group);

    // called by notificationCenter. Will call our virtual segmentUpdated()
    void updateSegment(const P2PNet::Notification &notification);

    /**
     * Turning on collation will remember segment updates and add the values
     * in the data structure you can retrieve with collatedData()
     */
    void setCollation(bool on);
    /// returns true if we are collating.
    bool isCollating() const;
    /// remove historical (collated) segment data.
    void flushCollate();
    /// return the collated per-segment data.
    std::vector<P2PNet::Notification> collatedData() const;

private:
    bool m_doCollate = false;
    std::vector<P2PNet::Notification> m_collatedData;
    mutable std::mutex m_lock;
};

#endif
