/*
 * This file is part of the Flowee project
 * Copyright (C) 2020-2021 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Action.h"
#include "DownloadManager.h"

#include <functional>

#include <boost/asio/bind_executor.hpp>

Action::Action(DownloadManager *parent)
    : m_dlm(parent),
    m_timer(parent->ioContext())
{
}

void Action::again()
{
    if (m_dlm->powerMode() != P2PNet::NormalPower) {
        m_dlm->done(this);
        return;
    }
    m_timer.expires_from_now(boost::posix_time::milliseconds(m_interval));
    m_timer.async_wait(boost::asio::bind_executor(m_dlm->strand(),
            std::bind(&Action::execute, this, std::placeholders::_1)));
}

void Action::setInterval(int milliseconds)
{
    m_interval = milliseconds;
}

void Action::start()
{
    assert(m_dlm);
    // first time we just call it, no timer. But in the strand.
    m_dlm->strand().post(std::bind(&Action::execute, this, boost::system::error_code()), std::allocator<void>());
}

void Action::cancel()
{
    m_timer.cancel();
}

Action::~Action()
{
}
