/*
 * This file is part of the Flowee project
 * Copyright (C) 2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_HEADERSYNCINTERFACE_H
#define FLOWEE_HEADERSYNCINTERFACE_H

/**
 * A listener on status updates for the headers-chain.
 *
 * \see DownloadManager::addHeaderListener(HeaderSyncInterface *listener)
 */
class HeaderSyncInterface
{
public:
    HeaderSyncInterface();

    /// notify when we get a newer (higher) blockheight
    virtual void setHeaderSyncHeight(int height);
    /// Callback when we move from a not yet synched client to one that now has headers till 'now'.
    // called at most once per application-run
    virtual void headerSyncComplete();
};

#endif
