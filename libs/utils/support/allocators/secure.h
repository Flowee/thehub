/*
 * This file is part of the Flowee project
 * Copyright (C) 2009-2010 Satoshi Nakamoto
 * Copyright (C) 2009-2015 The Bitcoin Core developers
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FLOWEE_SUPPORT_ALLOCATORS_SECURE_H
#define FLOWEE_SUPPORT_ALLOCATORS_SECURE_H

#include <support/cleanse.h>
#include <support/lockedpool.h>

#include <memory>
#include <string>

//
// Allocator that locks its contents from being paged
// out of memory and clears its contents before deletion.
//
template <typename T>
struct secure_allocator : public std::allocator<T> {
    using base = std::allocator<T>;
    using traits = std::allocator_traits<base>;
    using size_type = typename traits::size_type;
    using difference_type = typename traits::difference_type;
    using pointer = typename traits::pointer;
    using const_pointer = typename traits::const_pointer;
    using value_type = typename traits::value_type;
    secure_allocator() noexcept {}
    secure_allocator(const secure_allocator &a) noexcept : base(a) {}
    template <typename U>
    secure_allocator(const secure_allocator<U> &a) noexcept : base(a) {}
    ~secure_allocator() noexcept {}
    template <typename _Other> struct rebind {
        typedef secure_allocator<_Other> other;
    };

    T *allocate(std::size_t n, const void *hint [[maybe_unused]] = 0) {
        return static_cast<T *>(LockedPoolManager::instance().alloc(sizeof(T) * n));
    }

    void deallocate(T *p, std::size_t n) {
        if (p != nullptr) {
            memory_cleanse(p, sizeof(T) * n);
        }
        LockedPoolManager::instance().free(p);
    }
};

// This is exactly like std::string, but with a custom allocator.
using SecureString = std::basic_string<char, std::char_traits<char>, secure_allocator<char>>;

#endif
