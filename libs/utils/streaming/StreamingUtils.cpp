/*
 * This file is part of the Flowee project
 * Copyright (C) 2016,2018-2020 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "StreamingUtils.h"
#include <cstring>
#include <limits>

int Streaming::writeCompactSize(char *data, uint64_t value)
{
    int size = 1;
    if (value < 253) {
        // nothing to do
    }
    else if (value <= std::numeric_limits<unsigned short>::max()) {
        uint16_t copy = static_cast<uint16_t>(value);
        memcpy(data + 1, &copy, 2);
        size += 2;
        value = 253;
    }
    else if (value <= std::numeric_limits<unsigned int>::max()) {
        uint32_t copy = static_cast<uint32_t>(value);
        memcpy(data + 1, &copy, 4);
        size += 4;
        value = 254;
    }
    else {
        memcpy(data + 1, &value, 8);
        size += 8;
        value = 255;
    }
    *data = static_cast<uint8_t>(value);
    return size;
}


