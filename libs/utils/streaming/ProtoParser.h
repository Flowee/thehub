/*
 * This file is part of the Flowee project
 * Copyright (C) 2016-2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_PROTOPARSER_H
#define FLOWEE_PROTOPARSER_H

#include "MessageParser.h" //for the ParsedType enum
#include "ConstBuffer.h"
#include "../uint256.h"

namespace Streaming {

class ProtoParser
{
public:
    /// \internal
    enum WireType {
        VarInt,
        Int64,
        Data,
        Int32 = 5
    };

    ProtoParser(const ConstBuffer &buffer);

    ParsedType next();

    /// returns the tag of the current item.
    uint32_t tag() const;

    bool isInt() const;
    bool isLong() const;
    bool isByteArray() const;

    static void debug(const Streaming::ConstBuffer &data);

    int32_t intData() const;
    uint64_t longData() const;
    std::string stringData();
    //boost::string_ref rstringData() const;
    bool boolData() const;
    std::vector<char> bytesData() const;
    ConstBuffer bytesDataBuffer() const;
    /**
     * Open the current data as a new parser.
     * This is semi-equivalent to;
     *   ParserData newParser(oldParser.bytesDataBuffer());
     */
    void enterData();
    /// Lower the depth by one, if possible.
    void closeData();
    /// return the depth of nesting, no nesting means a depth of 1.
    int depth() const;
    /// Return the amount of bytes the string or byte-array is in length
    int dataLength() const;
    uint256 uint256Data() const;


private:
    const char *m_privData;
    const int m_length;
    int m_position = 0;

    WireType m_wireType = VarInt;
    uint32_t m_tag = 0;
    uint64_t m_value = 0;

    int m_dataStart = -1;
    int m_dataLength = -1;

    ConstBuffer m_constBuffer;

    std::unique_ptr<ProtoParser> m_nestedParser;
};
}
#endif
