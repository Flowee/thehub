/*
 * This file is part of the Flowee project
 * Copyright (C) 2016-2023 Tom Zander <tom@flowee.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef FLOWEE_PROTOBUILDER_H
#define FLOWEE_PROTOBUILDER_H

#include "ConstBuffer.h"

#include <uint256.h>

namespace Streaming {

class BufferPool;

class ProtoBuilder
{
public:
    ProtoBuilder(BufferPool &pool);
    ProtoBuilder(ProtoBuilder &o) = delete;

    void add(uint32_t tag, uint64_t value);
    /// add an utf8 string
    void add(uint32_t tag, const std::string &value);
    inline void add(uint32_t tag, const char *utf8String) {
        return add(tag, std::string(utf8String));
    }

    void add(uint32_t tag, const std::vector<char> &data);
    void add(uint32_t tag, const ConstBuffer &data);
    void addByteArray(uint32_t tag, const void *data, int bytes);
    void add(uint32_t tag, bool value);
    void add(uint32_t tag, int32_t value);
    void add(uint32_t tag, double value);
    template<unsigned int BITS>
    void add(uint32_t tag, const base_blob<BITS> &value) {
        addByteArray(tag, static_cast<const void*>(value.begin()), static_cast<int>(value.size()));
    }
    void add(uint32_t tag, const std::vector<uint8_t> &data) {
        addByteArray(tag, static_cast<const void*>(data.data()), static_cast<int>(data.size()));
    }
    void add(uint32_t tag, const std::vector<int8_t> &data) {
        addByteArray(tag, static_cast<const void*>(data.data()), static_cast<int>(data.size()));
    }
    inline void add(uint32_t tag, float value) {
        add(tag, (double) value);
    }
    /**
     * This allows you to embed a message inside of this message.
     * The nested message will be built and after that message is finished
     * it will be copied and included into this builder as a 'data' object
     * which will have the tag \a tag.
     *
     * Notice that adding anything to this builder or calling buffer() will
     * finish the nested builder.
     */
    ProtoBuilder *addNestedMessage(uint32_t tag);

    /**
     * @brief buffer returns a buffer object for the message we've built.
     */
    ConstBuffer buffer();

    ProtoBuilder* operator=(const ProtoBuilder &o) = delete;

private:
    void add(uint32_t tag, const unsigned char *data, unsigned int length);
    void handleNestedBuilder();

    BufferPool *m_buffer;
    ProtoBuilder *m_nestedBuilder = nullptr;
    BufferPool *m_nestedBufferPool = nullptr;
};

}
#endif
